# Bierlisten-Installer

## Was tut dieses Skript?
- Installiert JavaFX manuell auf Rasbpian (keine automatische Installation mehr seit Java 8u33 möglich)
- Installiert eloTouch (Single-Touch) Treiber für Touchscreen-Monitore dieses Herstellers
- Richtet BierlisteAVH.jar als systemd service ein
- Richtet ein Backup System ein, das mithilfe von GIT und Crontab einmal täglich die Daten auf ein Remote GIT Repository sichert

## Annahmen:
- Pi 3 (Raspbian Jessie) mit Internetverbindung (zumindest für die Installation)
- eloTouch Monitor
- Externer / Remote Git Server auf den per SSH Backups gepushed werden können

## Benötigt folgende Sachen:
- Installationsskript wird von "pi" user benutzt
- Dieses Repo (bierlisten-installer) wird ins Homeverzeichnis geklont, so dass die Dateien in "/home/pi/bierlisten-installer/*" liegen
- Die Bierlisten-Jar sollte zu finden sein an Position "/home/pi/bierliste/bierliste-binary/BierlisteAVH.jar" (kann vor oder nach Skript gemacht werden, führt bei Fehlen aber zu kleinerer Fehlermeldung in Skript)


## Wie führe ich die Installation durch:
- Führe aus "~/bierlisten-installer/bierliste_setup.sh"
    - Wird auch JavaFX und den eloTouch Treiber installieren
    - Installiert benötigte Dependencies per apt-get
- Es benötigt mehrere Nutzereingaben während der Installation:
    - GIT Server (SSH) Url (an dieser Stelle wird auch ggf. ein Public Key generiert und angezeigt, der sollte auf den Remote Server installiert werden)
    - Ein Branchname für Git: Jede Pi Installation sollte auf einen eigenen, einzigartigen Branch committed werden. Backups wiederherstellen kann dann per "git merge" geschehen
    - Am Ende wird um manuellen Neustart gebeten. Der Touch-Treiber wird beispielsweise erst mit einem Neustart geladen. Hierzu muss jedoch der Touchscreen per 
        - Der Touchscreen ist zu diesem Zeitpunkt nicht kalibriert. Dies muss ggf. manuell erneut geschehen. Hierzu sollte der Bierliste-systemd-service angehalten werden, um auf den Desktop booten zu können.