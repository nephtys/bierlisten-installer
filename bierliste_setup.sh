#!/bin/bash
echo "Installation der Bierliste beginnt:"
echo "Dieser Vorgang ist gedacht für einen Raspberry Pi 3 mit Raspbian Jessie in Kombination mit einem eloTouch Monitor"

mkdir ~/bierliste

#refresh package repositories
sudo apt-get update

#install relevant packages
sudo apt-get install openjdk-8-jdk git libusb-1.0.0 libusb-1.0.0-dev bc


#COMMENTED OUT BECAUSE WE DO NOT NEED THAT USER
#create user 'bierliste' if non existing
#echo "Creating system-user 'bierliste' if it does not yet exist..."
#id -u bierliste >/dev/null 2>&1 || useradd bierliste -m -r
#echo "Success"



#check if required things are all in place: elo driver tarball, javafx-arm.zip, (not BierlisteAVH.jar yet). If not, return with error
echo "Checking if required files exist at the right location..."

if [ ! -f ~/bierlisten-installer/javafx-rpi/armv6hf-sdk-8.60.9.zip ]; then
    echo "ERROR: armv6hf file not found at ~/bierlisten-installer/javafx-rpi/armv6hf-sdk-8.60.9.zip"
    exit 1
else   
if [ ! -f ~/bierlisten-installer/touchdriver/SW602480_Elo_Linux_ST_USB_Driver_v4.3.1_armv7l.tgz ]; then
    echo "ERROR: Elo_Linux_ST_USB_Driver file not found at ~/bierlisten-installer/touchdriver/SW602480_Elo_Linux_ST_USB_Driver_v4.3.1_armv7l.tgz"
    exit 1
fi
fi
echo "Success"

#if no pub key in ssh directory, create on silently
mkdir ~/.ssh
if [ ! -f ~/.ssh/id_rsa.pub ]; then
echo "Generating new ssh key"
ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
echo "Success!"
else 
echo "id_rsa.pub key already exists"
fi


#print out the new (or old) public key and ask user to add to git remote repo
echo "Found following public ssh key:"
printf '%b\n' "$(cat ~/.ssh/id_rsa.pub)"
echo "Add it to your remote git repo to enable ssh access from this machine."


#ask for git remote repo url as user input 
echo "Please write in a correct remote git repo url as user input, followed by [ENTER], like git@github.com:USERNAME/REPOSITORY.git:"
read remoteurl

echo "Read in url:"
echo remoteurl

echo "Please give this machine a new git branch name (one word, no spaces, does not start with digits!), followed by [ENTER], like 'abcdEFg':"
read branchname

echo "Read in branchname:"
echo branchname

#create gitignore
echo "bierliste-data/database/db_bierliste.h2.lock.db" > ~/bierliste/.gitignore


#init git repo and pull existing information in
git init ~/bierliste
cd ~/bierliste
git remote add origin $remoteurl
git config --global user.email "bierliste@av-huette.de"
git config --global user.name "Bierliste"
git checkout -b  $branchname
git fetch --all

#create git backup script
mkdir ~/bierliste/bierliste-config
rm ~/bierliste/bierliste-config/backup_via_git.sh
echo "#!/bin/bash" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "cd ~/bierliste" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "git add ~/bierliste/bierliste-config" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "git add ~/bierliste/bierliste-data" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "git add ~/bierliste/.gitignore" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "git commit -m 'daily backup'" >> ~/bierliste/bierliste-config/backup_via_git.sh
echo "git push --set-upstream origin $branchname" >> ~/bierliste/bierliste-config/backup_via_git.sh
chmod +x ~/bierliste/bierliste-config/backup_via_git.sh

#place git backup script into crontab
(crontab -l ; echo "10 10 * * * ~/bierliste/bierliste-config/backup_via_git.sh") | crontab -

#unzip javafx lib
mkdir ~/bierliste/bierliste-binary
unzip ~/bierlisten-installer/javafx-rpi/armv6hf-sdk-8.60.9.zip -d ~/bierliste/bierliste-binary

#install jfx jars locally
sudo chown -R root:root ~/bierliste/bierliste-binary/armv6hf-sdk
sudo cp ~/bierliste/bierliste-binary/armv6hf-sdk/lib/javafx-mx.jar /usr/lib/jvm/java-8-openjdk-armhf/lib/
sudo cp ~/bierliste/bierliste-binary/armv6hf-sdk/rt/lib/j* /usr/lib/jvm/java-8-openjdk-armhf/jre/lib/
sudo cp ~/bierliste/bierliste-binary/armv6hf-sdk/rt/lib/arm/* /usr/lib/jvm/java-8-openjdk-armhf/jre/lib/arm/
sudo cp ~/bierliste/bierliste-binary/armv6hf-sdk/rt/lib/ext/* /usr/lib/jvm/java-8-openjdk-armhf/jre/lib/ext/



#create systemd job for start Bierliste and enable (and show status)
echo "[Unit]
Description=Bierlisten Start Script (startet nach Boot einmal neu)

[Service]
Type=simple
ExecStart=/usr/bin/java -server -jar /home/pi/bierliste/bierliste-binary/BierlisteAVH.jar -c /home/pi/bierliste/bierliste-config -d /home/pi/bierliste/bierliste-data -l /home/pi/bierliste/bierliste-log  -t /tmp/bierliste-tmp 

[Install]
WantedBy=multi-user.target" > ~/bierliste/bierliste-config/start_bierliste.sh
chmod +x ~/bierliste/bierliste-config/start_bierliste.sh
sudo mv ~/bierliste/bierliste-config/start_bierliste.sh /etc/systemd/system/bierliste.service
sudo systemctl daemon-reload
sudo systemctl start bierliste
sudo systemctl status bierliste
sudo systemctl enable bierliste




#Following: Single Touch Driver (only for XServer)

#untar touch driver to tmpfs
#mkdir /tmp/elotouch
#sudo rm -rf /tmp/elotouch/bin-usb/
#tar xvzf ~/bierlisten-installer/touchdriver/SW602480_Elo_Linux_ST_USB_Driver_v4.3.1_armv7l.tgz -C /tmp/elotouch

#copy driver files from 'bin/usb' to /etc/opt/elo-usb
#sudo cp -r /tmp/elotouch/bin-usb/ /etc/opt/elo-usb
#sudo rm -rf /tmp/elotouch/bin-usb/

#set full permissions for everything in there
#sudo chmod -R 777 /etc/opt/elo-usb/*
#sudo chmod -R 444 /etc/opt/elo-usb/*.txt
#sudo cp /etc/opt/elo-usb/99-elotouch.rules /etc/udev/rules.d

#install elo touch systemd job
#sudo cp /etc/opt/elo-usb/elo.service /etc/systemd/system/
#sudo systemctl daemon-reload
#sudo systemctl enable elo.service
#sudo systemctl status elo.service


#Following: Multi Touch Driver

#untar touch driver to tmpfs
mkdir /tmp/elotouch
sudo rm -rf /tmp/elotouch/bin-usb/
tar xvzf ~/bierlisten-installer/touchdriver/Elo_Linux_MT_USB_Driver_v1.0.0_armv7l.tgz -C /tmp/elotouch

#copy driver files from 'bin/usb' to /etc/opt/elo-usb
sudo mkdir /etc/opt
sudo cp -r /tmp/elotouch/bin-usb/ /etc/opt/elo-usb
sudo rm -rf /tmp/elotouch/bin-usb/

#set full permissions for everything in there
sudo chmod -R 777 /etc/opt/elo-usb/*
sudo chmod -R 444 /etc/opt/elo-usb/*.txt
sudo cp /etc/opt/elo-usb/99-elotouch.rules /etc/udev/rules.d

#compile and build new kernel module TODO
sudo apt-get install raspberrypi-kernel-headers libncurses5-dev
mkdir /tmp/rpi-source
cd /tmp/rpi-source
sudo wget https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source -O /usr/bin/rpi-source && sudo chmod +x /usr/bin/rpi-source && /usr/bin/rpi-source -q --tag-update
rpi-source

cd /etc/opt/elo-usb/elo_mt_input_mod_src
sudo echo '#include <linux/slab.h>' | cat - elo_mt_input.c > temp && mv temp elo_mt_input.c

sudo make 
sudo make install


#configure touch driver script to run at system startup TODO

#echo "[Unit]
#Description=eloTouch Multitouch Driver Start Script (startet nach Boot einmal neu)
#
#[Service]
#Type=simple
#ExecStart=/etc/opt/elo-usb/loadEloMultiTouchUSB.sh
#
#[Install]
#WantedBy=multi-user.target" > ~/bierliste/bierliste-config/elomultitouchdriver.sh
#chmod +x ~/bierliste/bierliste-config/elomultitouchdriver.sh
#udo mv ~/bierliste/bierliste-config/elomultitouchdriver.sh /etc/systemd/system/elomultitouchdriver.service
#sudo systemctl daemon-reload
#sudo systemctl enable elomultitouchdriver
#sudo systemctl status elomultitouchdriver
echo "sudo /etc/opt/elo-usb/loadEloMultiTouchUSB.sh" >> ~/.profile

#potentially has to be calibrated on device via 
#cd /etc/opt/elo-usb
#./elova

#needs at least 256mbyte video ram


#ask for manual reboot (after checking console out for errors)
echo "Installation ist zu Ende. Überprüfe das Log auf Fehler und starte den Computer neu, z.B. mit: sudo reboot"
